#include <stdio.h>
#include <stdlib.h>
#include "piezas.h"
#include <time.h>
#include "reglas.h"

static time_t tiempo_ini;

const char arreglo_piezas_0[7][4][4] = {
	{{0, 0, 0, 0}, {1, 1, 1, 1}, {0, 0, 0, 0}, {0, 0, 0, 0}},
	{{0, 0, 0, 0}, {2, 2, 2, 0}, {0, 0, 2, 0}, {0, 0, 0, 0}}, 
	{{0, 0, 0, 0}, {0, 3, 3, 3}, {0, 3, 0, 0}, {0, 0, 0, 0}}, 
	{{0, 0, 0, 0}, {0, 4, 4, 0}, {0, 4, 4, 0}, {0, 0, 0, 0}}, 
	{{0, 0, 0, 0}, {0, 0, 5, 5}, {0, 5, 5, 0}, {0, 0, 0, 0}}, 
	{{0, 0, 0, 0}, {6, 6, 6, 0}, {0, 6, 0, 0}, {0, 0, 0, 0}}, 
	{{0, 0, 0, 0}, {7, 7, 0, 0}, {0, 7, 7, 0}, {0, 0, 0, 0}}
	};

const int wall_kick_matrix[4][4][2] = {
	{{-1, 0}, {-1, 1},  {0, -2}, {-1, -2}}, 
	{{1, 0},  {1, -1},  {0, 2},  {1, 2}}, 
	{{1, 0},  {1, 1},   {0, -2}, {1, -2}}, 
	{{-1, 0}, {-1, -1}, {0, 2},  {-1, 2}}
	};

const int wall_kick_matrix_i[4][4][2] = {
	{{-2, 0}, {1, 0}, {-2, -1}, {1, 2}}, 
	{{-1, 0}, {2, 0}, {-1, 2}, {2, -1}},
	{{2, 0}, {-1, 0}, {2, 1}, {-1, -2}},
	{{1, 0}, {-2, 0}, {1, -2}, {-2, 1}}
	};

bloque_t Crear_Pieza(void)
{
	int n_pieza = (rand() % 7) + 1;
	bloque_t pieza;

	pieza.tipo = n_pieza;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{

			pieza.matrix_pieza[i][j] = arreglo_piezas_0[n_pieza - 1][i][j] + PIEZA_MOVIL;
		}
	}

	pieza.rotacion = 0;
	pieza.columna = 4;
	pieza.fila = 1;
	return pieza;
}

void Girar_Pieza(bloque_t *pieza, char matris[][12]) // funcion para rotar una pieza
{
	if (pieza->tipo == PIEZA_O)
	{
		return;
	}

	bloque_t pieza_temp = *pieza;
	pieza_temp.rotacion++;
	pieza_temp.rotacion %= 4;

	for (int i = 0; i < 4; i++) // devolvemos la pieza a la matriz original rotada
	{
		for (int j = 0; j < 4; j++)
		{
			pieza_temp.matrix_pieza[j][3-i] = pieza->matrix_pieza[i][j]; // regla de rotacion
		}
	}
	printf("gira \n");
	if (choque(matris, &pieza_temp) == BIEN)
	{
		*pieza=pieza_temp;
	}
	else if (wall_kick(&pieza_temp, matris) == BIEN)
	{
		printf("gira wall kick \n");
		*pieza=pieza_temp;
	}
}

int choque(char matris[][12], bloque_t *pieza)
{
	int conflicto = BIEN;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if ((matris[pieza->fila + i][pieza->columna + j] != 0) && (pieza->matrix_pieza[i][j] > 10))
			{
				conflicto = MAL;
				printf("choca\n");
				return conflicto;
			}
		}
	}
	return conflicto;
}

void Mover_Pieza(bloque_t *pieza, int direccion, char matris[][12])
{

	bloque_t pieza_temp = *pieza;
	pieza_temp.columna = pieza_temp.columna + direccion;

	if (choque(matris, &pieza_temp) == BIEN)
	{
		*pieza = pieza_temp;
	}
}

void Bajar_Pieza(bloque_t *pieza, char matris[][12])
{
	bloque_t pieza_temp = *pieza;
	pieza_temp.fila = pieza_temp.fila + 1;

	if (choque(matris, &pieza_temp) == BIEN)
	{
		*pieza = pieza_temp;
	}
}

void Estacionar(bloque_t *pieza, char matris[][12])
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if (matris[pieza->fila + i][pieza->columna + j] == NADA)
			{
				matris[pieza->fila + i][pieza->columna + j] = (pieza->matrix_pieza[i][j] - PIEZA_MOVIL);
			}
		}
	}
}

int wall_kick(bloque_t *pieza, char matris[][12])
{
	int conflicto = MAL;			//inicializo variables
	int rotacion = pieza->rotacion;
	int prueba=0;

	if (pieza->tipo == PIEZA_I)		//separo el caso especial de la pieza I
	{
		while (prueba < 4)	//corre hasta que se agoten las pruebas o el conflicto de BIEN
		{
			pieza->fila += wall_kick_matrix_i[rotacion][prueba][0];	//desplazamiento en x
			pieza->columna += wall_kick_matrix_i[rotacion][prueba][1];		//desplazamiento en y
			conflicto = choque(matris, pieza);							//chequeo si se resolvio el choque
			if (conflicto == BIEN)	//si el choque ya se resolvio nos devuelve el desplazamiento y nos avisa
			{
				return conflicto;
			}
			pieza->fila -= wall_kick_matrix_i[rotacion][prueba][0];	//quito desplazamientos
			pieza->columna -= wall_kick_matrix_i[rotacion][prueba][1];
			prueba++;
		}
	}
	else
	{
		while (prueba < 4)	//mismo que con el caso especial salvo el cambio de matris
		{
			printf("entra al wall kick \n");
			pieza->fila += wall_kick_matrix[rotacion][prueba][0];
			pieza->columna += wall_kick_matrix[rotacion][prueba][1];
			conflicto = choque(matris, pieza);
			if (conflicto == BIEN)
			{
				return conflicto;
			}
			pieza->fila -= wall_kick_matrix[rotacion][prueba][0];
			pieza->columna -= wall_kick_matrix[rotacion][prueba][1];
			prueba++;
		}
	}
	return conflicto;
}

int borrarFila (char tablero[18][12])
{
	int puntaje=0;

	for (int i = 16; i >=1 ; i--)
	{
		int contador=0;
		int j;
		for (j = 0; j < 11; i++)
		{
			if (tablero[i][j]>0)
			{
				contador++;
			}
			
		}
		if (contador=10)
		{	
			for (int m = 1; m < 11; i++)
			{
				tablero[i][m]=NADA;				
			}
			puntaje+=1000;
			for (int k=i; i >=0 ; k--)
			{
				int contador=0;
				for (int g = j; g < 11; g++)
				{
					if (tablero[k-1][g]!=BORDE)
					{
						tablero[k][g]=tablero[k-1][g];
					}
				}	
			}
		}
		
		
	}
	return puntaje;
}


void inicializarTiempo(void) 
{
    tiempo_ini = time(NULL);
}

double tiempo_transcurrido(void)
{
	return difftime(tiempo_ini,time(NULL));
}