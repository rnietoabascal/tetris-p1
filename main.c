/*HEADERS*/
#include "piezas.h"

/*ALLEGRO*/
#include "inicializacion_al.h"
#include "interfaz.h"               //mati toco esto <3

/**/
/*BIBLIOTECAS STANDARD*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct
{
    ALLEGRO_EVENT ev;
    bool close_display;
    bool menu;
    bool game_sel;
    bool record;
    bool game;
}state_t;

int main(int argc, char * argv[])
{
    srand(time(NULL));
    //ELEMENTOS DE ALLEGRO
    element_t elem = {NULL, NULL, NULL, NULL, NULL, NULL, NULL}; 
    state_t estado = {.close_display = false, .menu = true, .game=false};
    
    if(inicializa_al(&elem) == EXIT_FAILURE)
    {
        printf("ERROR: no se pudieron inicializar los elementos de Allegro\n");
        return EXIT_FAILURE; //si no pudo inicilizar correctamente, 
    }
    
    
    
    /*MENU*/
    menu_state_t estado_menu = p_menu(&elem);
    estado.close_display = estado_menu.window;
    estado.game_sel = estado_menu.play;
    estado.record = estado_menu.record;
    
    
    while(!estado.close_display)
    {
        if(al_get_next_event(elem.event_queue, &estado.ev))
        {
            /*EVENTOS DE DISPLAY*/
            if(estado.ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
            {
                estado.close_display = true;
            }
        }
    }

    int n = 0;
    char matris [18][12];         //creamos la matris del juego
    for (int i = 0; i < 18; i++)  //llena de 0 la matriz el interior y define los bordes
    {
        for (int j = 0; j < 12; j++)
        {
            if (i=0 || i=11 || j=0 || j=17)
            {
                matris[i][j]=BORDE;
            }
            else
            {
                matris[i][j]=NADA;
            }
            
        }
    }
    
    return EXIT_SUCCESS;
    

}

