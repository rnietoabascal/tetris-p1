#ifndef PIEZAS_H
#define PIEZAS_H

#define BORDE   100
#define NADA    0
#define PIEZA_I 1       //palo
#define PIEZA_J 2       //j
#define PIEZA_L 3       //L
#define PIEZA_O 4       //cuadrado
#define PIEZA_S 5       //s
#define PIEZA_T 6       //T
#define PIEZA_Z 7       //z
#define PIEZA_MOVIL 10

extern const char arreglo_piezas_0 [7][4][4];

                                  
typedef struct
{
    int tipo;
    char matrix_pieza [4][4];
    int columna;
    int fila;
    int rotacion;
} bloque_t;

#endif