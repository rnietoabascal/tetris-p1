#ifndef INICIALIZACION_AL_H
#define INICIALIZACION_AL_H

/*BIBLIOTECAS DE ALEGRO*/
#include <allegro5/allegro_color.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_primitives.h>
#include "allegro5/allegro_ttf.h"
#include <allegro5/allegro_audio.h> 
#include <allegro5/allegro_acodec.h> //Extensiones con acodec .wav, .flac, .ogg, .it, .mod, .s3m, .xm.

/*MACROS*/

/*TAMAÑO DE LA PANTALLA*/
#define SCREEN_W  1260
#define SCREEN_H  720


/*ESTRUCTURAS*/
typedef struct
{
    ALLEGRO_DISPLAY *display;
    ALLEGRO_EVENT_QUEUE *event_queue;
    ALLEGRO_TIMER *timer;
    ALLEGRO_FONT *title;
    ALLEGRO_FONT *buttons;
    ALLEGRO_SAMPLE *sample_menu;
    ALLEGRO_SAMPLE *sample2;
    ALLEGRO_SAMPLE *sample3;
}element_t;

/*PROTOTIPOS*/
int inicializa_al(element_t* elem); 

#endif /* INICIALIZACION_AL_H */

